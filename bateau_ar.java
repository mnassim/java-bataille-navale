import java.awt.Graphics;
import java.awt.Color;
import java.lang.*;
import java.awt.*;
import java.awt.geom.Area;
import java.util.ArrayList;
import java.awt.geom.Ellipse2D;

class bateau_ar extends Objet_marin{// bateau armee
		private int nb = 1;

    public int getNb() {
        return nb;
    }

    public void setNb(int nb) {
        this.nb = nb;
    }

    bateau_ar(int x, int y, double dir, int vitesse){
 	super(Color.GREEN,20,7,x,y,dir,vitesse);
 }


boolean detection(sous_marin o){//detection du sous marin
	Ellipse2D.Double form =new Ellipse2D.Double(this.x2-350,this.y2-350,700,700);
	Area radar=new Area(form);
	int []xtab1={o.x,o.x1,o.x2,o.x3,o.x4,o.x};
    int []ytab1={o.y,o.y1,o.y2,o.y3,o.y4,o.y};
    Polygon p1= new Polygon(xtab1,ytab1,5);
    Area soum= new Area(p1);
     radar.intersect(soum);
       if (!radar.isEmpty() && o.profondeur<15)
       {
       	double angle = Math.atan2(this.y-o.y,this.x-o.x)-Math.atan2(this.y-this.y1, this.x-this.x1);
      	this.dir=this.dir+angle;
      	return true;
       }
       return false;
}
}
