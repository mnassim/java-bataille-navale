import java.awt.Graphics;
import java.awt.Color;
import java.lang.*;
import java.awt.*;
import java.awt.geom.Area;
import java.util.ArrayList;


class Objet_marin{
Color couleur;
int vitesse;
double dir;
int lo;
int la;
int x,y,x1,x2,x3,x4,y1,y2,y3,y4;//coordonee
int vie;

Objet_marin(Color c,int longueur,int largeur,int x,int y,double dir,int vitesse){
this.x=x;
this.y=y;
this.lo=longueur;
this.la=largeur;
this.couleur=c;
this.vitesse=vitesse;
this.dir=dir;
this.vie=1;
		x1= (int) (this.x +(this.lo*Math.cos(dir)));
        	y1= (int)(this.y + (this.lo*Math.sin(dir)));

        	x3= (int) (x1 - la*Math.cos((Math.PI)/2 - dir));
        	y3= (int) (y1 + la*Math.sin((Math.PI)/2 - dir));

        	x4= (int) (this.x - la*Math.cos((Math.PI)/2 - dir));
        	y4= (int) (this.y + la*Math.sin((Math.PI)/2 - dir));

        	x2= (int) ((x1 + x3)/2 + Math.cos(dir)*lo/4);
        	y2= (int) ((y1 + y3)/2 + Math.sin(dir)*lo/4);
}
  void paint(Graphics g){//affichage

		
                 
                int []xtab={this.x,this.x1,this.x2,this.x3,this.x4,this.x};
                int []ytab={this.y,this.y1,this.y2,this.y3,this.y4,this.y};
		g.setColor(this.couleur);
		g.fillPolygon(xtab,ytab,6);
		g.drawLine((x+x4)/2,(y+y4)/2,((x+x4)/2-(int)(Math.cos(dir)*this.vitesse)),((y+y4)/2-(int)(Math.sin(dir)*this.vitesse)));
		g.setColor(Color.BLACK);
		g.drawPolygon(xtab,ytab,6);
	}
   void deplacer(){
 		this.x= (int) (this.x +(this.vitesse*Math.cos(dir)));
        	this.y= (int)(this.y + (this.vitesse*Math.sin(dir))); 
		x1= (int) (this.x +(this.lo*Math.cos(dir)));
        	y1= (int)(this.y + (this.lo*Math.sin(dir)));

        	x3= (int) (x1 - la*Math.cos((Math.PI)/2 - dir));
        	y3= (int) (y1 + la*Math.sin((Math.PI)/2 - dir));

        	x4= (int) (this.x - la*Math.cos((Math.PI)/2 - dir));
        	y4= (int) (this.y + la*Math.sin((Math.PI)/2 - dir));

        	x2= (int) ((x1 + x3)/2 + Math.cos(dir)*lo/4);
        	y2= (int) ((y1 + y3)/2 + Math.sin(dir)*lo/4);       
        }
     boolean collision(Objet_marin o){
        int []xtab={this.x,this.x1,this.x2,this.x3,this.x4,this.x};
        int []ytab={this.y,this.y1,this.y2,this.y3,this.y4,this.y};
        Polygon p1= new Polygon(xtab,ytab,5);
        int []xtab1={o.x,o.x1,o.x2,o.x3,o.x4,o.x};
        int []ytab1={o.y,o.y1,o.y2,o.y3,o.y4,o.y};
        Polygon p2= new Polygon(xtab1,ytab1,5);
        Area a1= new Area(p1);
        Area a2= new Area(p2);
        a1.intersect(a2);
       if (!a1.isEmpty())//en cas de collision perte de vie
       {
        if(o.vie!=0)
        o.vie=o.vie-1;
        if(this.vie!=0);
        this.vie=this.vie-1;
        return true;
       }

return false;
     }   

}
