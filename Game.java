import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.Color;
import java.util.ArrayList;
import java.awt.*;
import java.awt.geom.Area;
import java.util.ArrayList;

public class Game {//class qui permet de gerer les evenement du jeu

	private fenetre frame;
	private int w,h;
	private flotte f;
	private    sous_marin s;
	private ArrayList<bombe> bombe ;
	private int score;
	private   bonus bonus;
	private boolean start = false;
     
        

	public Game(){//constructeur
		this.w = w;
		this.h = h;
		this.f = new flotte();
		this.frame = new fenetre(this);
                this.s=new sous_marin();
                this.bombe= new ArrayList<bombe>();
                this.score=0;
                this.bonus=this.bonus=new bonus((int)(Math.random()*800 +200),(int)(Math.random()*400+200));

                	}
        void tirer(int i){//permet de tirer des bombes
        	if(i==1)//bombe tiré par le sous-marin
        	{
        	if(this.s.nb>0 && this.s.profondeur==0){
         this.bombe.add(new bombe(this.s.dir,this.s.x,this.s.y,false));
     this.s.nb-=1;}}
     else{//bombe tire par le bateau enemie
     	this.bombe.add(new bombe(this.f.b.dir,this.f.b.x,this.f.b.y,true));
     	this.f.b.setNb(1);
     }
 
        }
       public  void gest_collison(){//gere les collision entre les differents elements
        	for(int i=0;i<3;i++){
for(int j=0;j<3;j++){
	if(this.f.fl[i][j].vie!=0 && this.s.profondeur==0){//colision sous-marin et bateau
if(this.s.collision(this.f.fl[i][j])) score+=200;
for(bombe ro : this.bombe){//collision bombe bateau
      if(ro.vie>0 && ! ro.isEnemy()) if(ro.collision(this.f.fl[i][j])){score+=400;
      ro.vie-=1;   }
               }
}

}
}
if (this.f.b.vie!=0 && this.s.profondeur==0){//colision sousmarin bateau armee
if(this.s.collision(this.f.b)) score+=300;
for(bombe ro : this.bombe){//collision bombe bateau armee
     if(ro.vie>0 && !ro.isEnemy()) if(ro.collision(this.f.b)){score+=400; }
               }
}
if (this.s.profondeur==0)
for(bombe ro : this.bombe){
	if(ro.vie>0 && ro.isEnemy()){
      ro.collision(this.s);//colision sous marin bombe
      }
               }

if (this.s.profondeur==0)//collision bonus sous marin
if(bonus.collision(s)){
	this.s.nb+=1;
	 this.bonus=new bonus((int)(Math.random()*800 +200),(int)(Math.random()*400+200));//nouveau bonus
score+=100;
}


}

        



	public void run(){//gestion des evenement
		while(this.s.vie!=0){
			try{Thread.sleep(75);}catch(InterruptedException e){}
			this.frame.repaint();//rafraichissement
			if(start){
this.f.reinit();
this.s.deplacer();
this.f.deplacer();
if(this.f.b.detection(this.s) && this.f.b.getNb()==1) tirer(2);
for(bombe ro : this.bombe){
               	ro.deplacer();
               }
               gest_collison();
		
		}
		this.frame.repaint();//rafraichissement
		}
	}


	public static void main(String[] toto){//main
		Game cg = new Game();
		cg.run();
	}

	public fenetre getFrame() {
		return frame;
	}

	public void setFrame(fenetre frame) {
		this.frame = frame;
	}

	public int getW() {
		return w;
	}

	public void setW(int w) {
		this.w = w;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	public flotte getF() {
		return f;
	}

	public void setF(flotte f) {
		this.f = f;
	}

	public sous_marin getS() {
		return s;
	}

	public void setS(sous_marin s) {
		this.s = s;
	}

	public ArrayList<bombe> getBombe() {
		return bombe;
	}

	public void setBombe(ArrayList<bombe> bombe) {
		this.bombe = bombe;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public bonus getBonus() {
		return bonus;
	}

	public void setBonus(bonus bonus) {
		this.bonus = bonus;
	}

	public boolean isStart() {
		return start;
	}

	public void setStart(boolean start) {
		this.start = start;
	}
}
