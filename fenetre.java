import javax.swing.JFrame;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;
import java.awt.event.*;


public class fenetre extends JFrame{

	private Game game;

	public fenetre(Game g){
		super("jeu");
		this.game = g;
		this.setBackground(Color.BLUE);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1000,800);
		this.setResizable(false);
		this.setVisible(true);
this.addKeyListener(new KeyAdapter(){//gestion du clavier
public void keyPressed(KeyEvent e){
if(e.getKeyCode()==KeyEvent.VK_SPACE) game.tirer(1);
else if (e.getKeyCode()==KeyEvent.VK_UP) game.getS().vitesse(1);
else if (e.getKeyCode()==KeyEvent.VK_LEFT) game.getS().changedir(1);
else if (e.getKeyCode()==KeyEvent.VK_RIGHT) game.getS().changedir(2);
else if (e.getKeyCode()==KeyEvent.VK_C) game.getS().prof(1);
else if (e.getKeyCode()==KeyEvent.VK_V) game.getS().prof(2);
else if (e.getKeyCode()==KeyEvent.VK_ENTER) {game.setStart(true);}

}
public void keyReleased(KeyEvent e){
if(e.getKeyCode()==KeyEvent.VK_UP) game.getS().vitesse(0);}

});
}


	public void paint(Graphics gr){//affichage
		
		 sous_marin s=this.game.getS();
                ArrayList<bombe> r=this.game.getBombe();
                bonus b=this.game.getBonus();
                if (this.game.isStart()){//quand la game commence
		if(s.vie!=0){
		flotte t = this.game.getF();
		gr.setColor(Color.BLUE);
		gr.fillRect(0,0,this.getWidth(),this.getHeight());
                gr.setColor(Color.YELLOW);
                gr.setFont(new Font("TimesRoman ",Font.BOLD,10));
                gr.drawString("votre score : "+this.game.getScore(),50,50);
                gr.drawString("votre prof : "+s.profondeur,50,65);
               gr.drawString("coordonées du sous marin : ("+s.x2+","+s.y2+")",400,50);
               gr.drawString("nombre de vies : ("+s.vie+")",800,50);
               gr.drawString("nombre de bombes : ("+s.nb+")",800,65);

        if(s.vie!=0) s.paint(gr);
        b.paint(gr);
        if(t.b.vie!=0) t.b.paint(gr);
               for(bombe ro : r){
               	 if (ro.vie>0)ro.paint(gr);

               }
for(int i=0;i<3;i++)
for(int j=0;j<3;j++){
		if(t.fl[i][j].vie!=0)t.fl[i][j].paint(gr);
	}
}
else {//a la fin
	gr.setColor(Color.BLACK);
			gr.fillRect(0,0,this.getWidth(),this.getHeight());
			gr.setColor(Color.WHITE);
                gr.setFont(new Font("TimesRoman ",Font.BOLD,50));
                gr.drawString("GAME OVER",350,300);
                 gr.setFont(new Font("TimesRoman ",Font.BOLD,25));
                gr.drawString("votre score : "+this.game.getScore(),400,500);
                //gr.drawString("tap r to retry ",400,600);
}
}
else{//au debut
	gr.setColor(Color.BLACK);
			gr.fillRect(0,0,this.getWidth(),this.getHeight());
			gr.setColor(Color.WHITE);
                gr.setFont(new Font("TimesRoman ",Font.BOLD,50));
                gr.drawString("bienvenue",350,100);
                 gr.setFont(new Font("TimesRoman ",Font.BOLD,25));
                gr.drawString("- vous devez detruire le plus de bateaux possibles",200,200);
                gr.drawString("- utilisez les touches directionelles pour vous deplace",200,300);
                gr.drawString("- espace pour tirer",200,400);
                gr.drawString("- c pour descendre et v pour remonter",200,500);
                gr.drawString("tapez entrez pour commencer a jouer",300,600);
}
	
}
	

}
